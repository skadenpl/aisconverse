<?php get_header(); ?>

    <section class="main">

        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-none">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                    aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <form>
                    <input class="form-control" type="search" placeholder="Search">
                </form>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <?php 
                    wp_nav_menu( [
                            'theme_location'  => '',
                            'menu'            => 'main-menu', 
                            'container'       => 'ul', 
                            'container_class' => 'menu clearfix', 
                            'container_id'    => 'mainMenu',
                            'menu_class'      => 'navbar-nav ml-auto', 
                            'menu_id'         => '',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul class="navbar-nav ml-auto">%3$s</ul>',
                            'depth'           => 0,
                            'walker'          => '',
                        ] ); ?>
                </div>

                <button class="btn">Contact</button>
            </nav>
        </div>


        <div class="container">

            <?php 
                $posts = get_field('carousel_slide');                
                if( $posts ): 
            ?>

            <ul id="main-carousel" class="carousel slide" data-ride="carousel">
            <?php foreach( $posts as $post): ?>
            <?php setup_postdata($post); ?>
                <div class="carousel-inner">
                    <li class="carousel-item">
                        <div class="row">
                            <div class="col-12 col-md-5 carousel_text_area">
                                <div class="carousel_date">
                                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/img/time.png);" alt="time">
                                    <?php the_field('slide_date'); ?>
                                </div>
                                <div class="carousel_text">
                                    <?php the_field('slide_text'); ?>
                                </div>
                                <button>Read more</button>
                            </div>
                            <div class="col-12 col-md-7">
                                <img src="<?php the_field('slide_image'); ?>" alt="first image" class="carosel_img">
                            </div>
                        </div>
                    </li>
                </div>
                <a class="carousel_control_prev" href="#main-carousel" role="button" data-slide="prev">
                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/img/arrow-prev.png);" alt="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel_control_next" href="#main-carousel" role="button" data-slide="next">
                    <img src="<?php bloginfo( 'template_url' ); ?>/assets/img/arrow-next.png);" alt="prev">
                    <span class="sr-only">Next</span>
                </a>
                <?php endforeach; ?>
            </ul>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>

        </div>
    </section>

    <section>
        <div class="container information">
            <div class="information_about">
                <h3><?php the_field('about_us_heading'); ?></h3>
                <div class="aboutUs_text">
                    <?php the_field('about_us_text'); ?>"
                </div>
                <hr>
            </div>
            <div></div>
            <div class="information_news">
                <h3><?php the_field('company_news_heading'); ?></h3>  

                <?php
                    global $post;
                    $args = array( 'numberposts' => 3 , 'category' => 4, 'orderby' => 'date');
                    $myposts = get_posts( $args );
                    foreach( $myposts as $post ){ setup_postdata($post);
                    ?>
                    <div class="news_post">
                        <div class="post_img">
                            <img src="<?php echo get_the_post_thumbnail_url( $page->ID ); ?>" class="img-fluid" alt="">
                        </div>
                        <div class="post_content">
                            <div class="post_date">
                               <?php echo get_the_date('d, M, Y'); ?>
                            </div>
                            <div class="post_text">
                                <?php the_title(); ?>
                            </div>
                        </div>                                          
                    </div>
                    <?php
                        }
                        wp_reset_postdata();
                    ?>      

                <!-- <div class="news_post">
                    <div class="post_img">
                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/img/post_image_1.png);" class="img-fluid" alt="">
                    </div>
                    <div class="post_content">
                        <div class="post_date">
                            26, November 2019
                        </div>
                        <div class="post_text">
                            <?php the_title(); ?>
                        </div>
                    </div>
                </div>
                <div class="news_post">
                    <div class="post_img">
                        <img src="<?php bloginfo( 'template_url' ); ?>/assets/img/post_image_2.png);" class="img-fluid" alt="">
                    </div>
                    <div class="post_content">
                        <div class="post_date">
                            20, September 2019
                        </div>
                        <div class="post_text">
                            Growing the influence of student voice over the essential elements of teaching and learning has always been appealing for school leaders.
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>

    <?php get_footer(); ?>

