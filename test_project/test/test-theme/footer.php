		<footer>
		  <div class="logo">
		      <img src="<?php the_field('company_logo'); ?>" alt="logo">
		      <?php the_field('company_name'); ?>
		  </div>
		</footer>

		<?php wp_footer(); ?>

	</body>

</html>