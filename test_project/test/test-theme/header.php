<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title><?php bloginfo( 'description ' ); ?></title>
    <?php wp_head(); ?>
</head>

<body>
    <header>
        <div class="container header_inner">
            <div class="logo">
                <img src="<?php the_field('company_logo'); ?>" alt="logo">
                <?php the_field('company_name'); ?>
            </div>
            <div class="phone_number">
                <hr class="align-self-center">
                <?php the_field('company_phone_number'); ?>
            </div>
        </div>
    </header>