<?php
/*
Template Name: Custom
Template Post Type: post, page, product
*/
?>

<?php get_header(); ?>

<div class="container">
	<?php woocommerce_content(); ?>
</div>

<?php get_footer(); ?>	