<?php 

add_theme_support( 'post-thumbnails' );

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
add_theme_support( 'woocommerce' );
}

add_action( 'wp_enqueue_scripts', 'test_style' );
add_action( 'wp_enqueue_scripts', 'test_scripts' );



function test_style() {

	wp_enqueue_style( 'main-style', get_stylesheet_uri() );

}

function test_scripts(){

	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js' );
	wp_enqueue_script( 'jquery' );

  wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/assets/js/bootstrap.js' );
	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/main.js',
	array(jquery), null, true);
	
}

 ?>